<!DOCTYPE html>
<html lang="pt-br">
  	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Codemax Sistemas - Criação de Sites, WordPress, Sistemas Web e Projetos Customizados</title>

		<meta name="abstract" content="criação de sites | criação de sites wordpress| criação de sites rj| criação de sites no rj| criação de sites rio| criação de sites rio de janeiro| criação de sites no rio de janeiro| criação de websites | criação de websites rj| criação de websites rio de janeiro| criar site| criamos site| criamos sites| como criar um site| criação de blog| criação de portal| criação de portais| criação de site responsivo| criação de site gerenciável| criação de site empresarial| agência de criação de sites| agência de criação de sites rj| agência de criação de sites rio de janeiro| agencia web| empresa sites| empresa que faz site| empresa de sites| desenvolvimento de sites| desenvolvimento de sites rj| construção de sites| construção de websites| criação de loja virtual| criação de e-commerce| criação de lojas virtuais| desenvolvimento web| atualização de sites| manutenção de sites| criação de sites institucionais| sites empresariais| sites profissionais| quanto custa um site| valores sites| empresa de criação de site| websites dinâmicos| site para empresas| web designer| web design| sites gerenciáveis| site para advogados| site para clinicas| site para escritórios| hospedagem| hospedagem de sites| registro de domínio| atualização e manutenção de sites">

		<meta name="keywords" itemprop="keywords" content="Empresa de criação de site em São Gonçalo, Empresa de criação de site em Niterói, Empresa de criação de site em Itaboraí, Empresa de criação de site em Maricá, criar site, fazer site, criação de site, Loja Virtual Niterói, Loja Virtual São Gonçalo, Loja Virtual Itaboraí, Loja Virtual Maricá, Web Site RJ, Web Site São Gonçalo, Web Site Niterói, Web Site Maricá, Web Site Itaboraí, Otimização de site, Email Marketing São Gonçalo, Email Marketing Itaboraí, Email Marketing Maricá, Email Marketing Niterói, Criação Gráfica São Gonçalo, Criação Gráfica Niterói, Criação Gráfica Itaboraí, Criação Gráfica Maricá" />

		<meta name="keywords" content="criação de sites, criação de sites wordpress, criação de sites rj, criação de sites no rj, criação de sites rio, criação de sites rio de janeiro, criação de sites no rio de janeiro, criação de websites, criação de websites rj, criação de websites rio de janeiro, criar site, criamos site, criamos sites, como criar um site, criação de blog, criação de portal, criação de portais, criação de site responsivo, criação de site gerenciável, criação de site empresarial, agência de criação de sites, agência de criação de sites rj, agência de criação de sites rio de janeiro, agencia web, empresa sites, empresa que faz site, empresa de sites, desenvolvimento de sites, desenvolvimento de sites rj, construção de sites, construção de websites, criação de loja virtual, criação de e-commerce, criação de lojas virtuais, desenvolvimento web, atualização de sites, manutenção de sites, criação de sites institucionais, sites empresariais, sites profissionais, quanto custa um site, valores sites, empresa de criação de site, websites dinâmicos, site para empresas, web designer, web design, sites gerenciáveis, site para advogados, site para clinicas, site para escritórios, hospedagem, hospedagem de sites, registro de domínio, atualização e manutenção de sites." />

		<meta name="description" content="Criação de Sites e Desenvolvimento de Sistemas em Rio de Janeiro, Itaboraí, São Gonçalo, Niterói, RJ, SP, São Paulo">

		<meta name="title" content="Empresa de criação de sistes no rio de janeiro - contato@codemax.com.br - especialista em wordpress - loja magento - Centro de São Gonçalo">

        <meta name="robots" content="index,follow">
        <meta name='target' content="all">
        <meta name="googlebot" content="index,follow">
        <meta name="author" content="Codemax Sistemas | contato@codemax.com.br" />
        <meta name="author-url" content="http://www.codemax.com.br" />
        <meta name="url" content="http://www.codemax.com.br">

		<meta name="identifier-URL" content="http://www.codemax.com.br">
        <meta name="organization name" content="Codemax Sistemas - contato@codemax.com.br">
        <meta name="revisit-after" content="2 days"/>
		<meta name="category" content="internet">
		<meta name="language" content="Portuguese">
		<meta name="doc-type" content="Web Page">
		<meta name="doc-rights" content="Copywritten Work">
		<meta name="resource-type" content="document">
		<meta name="classification" content="Sites">
		<meta name="classification" content="Sistemas">
		<meta name="classification" content="Aplicativos Mobile">
		<meta name="classification" content="Wordpress">
		<meta name="msvalidate.01" content="23DD47A1C7126648335234EFC43EC77E" />
		<meta name="google-site-verification" content="h6njSq7FvqbxhDax8qHVaOeqnmNzgZAsN3LFWhBwpxo" />
		<meta name="location" content="Rio de Janeiro | Brazil">
		<link rel="alternate" type="application/xml" title="Sitemap" href="http://www.codemax.com.br/sitemap.xml">
		<link rel="alternate" type="application/html" title="HTML" href="http://www.codemax.com.br/sitemap.html">
		<link rel="canonical" href="http://www.codemax.com.br" />
		<meta http-equiv="Expires" content="0">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Cache-Control" content="no-cache">

		<meta property="og:title" content="Criação de Sites no Rio de Janeiro - Wordpress - Sistemas web">
		<meta property="og:type" content="website">
		<meta property="og:locale" content="pt_BR">
		<meta property="og:url" content="http://www.codemax.com.br">
		<meta property="og:image" content="http://www.codemax.com.br/images/logo.png">
		<meta property="og:site_name" content="Codemax Sistemas">
		<meta property="fb:app_id" content="262843197416323">
		<meta property="fb:page_id" content="codemaxbr" />
		<meta property="og:description" content="Empresa de Criação de Sites e Desenvolvimento de Sites no Rio de Janeiro RJ, Entregamos seu site em até 7 dias, Especialistas em WordPress e Sistemas Web">
		<meta property="og:locality" content="Rio de Janeiro">
		<meta property="og:region" content="RJ">
		<meta property="og:country-name" content="Brasil">
		<meta property="og:email" content="contato@codemax.com.br">
		<meta property="og:phone_number" content="+55 21 9681-7475">

		<meta name="twitter:site" content="http://www.codemax.com.br"/>
		<meta name="twitter:domain" content="https://twitter.com/codemaxbr"/>
		<meta name="twitter:creator" content="Codemax Sistemas"/>
		<meta name="twitter:image:src" content="http://www.codemax.com.br/images/logo.png"/>

		<meta name="norton-safeweb-site-verification" content="6vnso78y3r6l3q3q43dog8juxvq4l60kjq44omvnu5fzkj1rbbvsva6rvxsy55qatdi11w-nto36o5flrf96uq8jm8cddzodxf2ienhv0vu9zq-pkvzse0lm-nx5rw0h" />

        <link rel="shortcut icon" href="images/favicon.ico">

		<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700,900,400italic' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Raleway:500,700,400' rel='stylesheet' type='text/css'>

		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="style.css">
		<link rel="stylesheet" type="text/css" href="fonts/flaticon.css">
		<link rel="stylesheet" type="text/css" href="css/unslider.css">

		<!-- HTML5 shim e Respond.js para suporte no IE8 de elementos HTML5 e media queries -->
		<!-- ALERTA: Respond.js não funciona se você visualizar uma página file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
  	</head>
  	<body>
  		<!-- Header -->
  		<header>
  			<div class="container">
  				<div class="col-md-4">
  					<img class="img-responsive" src="images/logo.png" />
  				</div>

  				<div class="col-md-8">
  					<div class="top_contact text-right">
  						<a href="#"><i class="fa fa-facebook-square"></i></a>
  						<a href="#"><i class="fa fa-skype"></i></a>
  						<a href="#"><i class="fa fa-whatsapp"></i></a>

  						(21) 97681-7475
  					</div>

  					<div class="ddsmoothmenu pull-right" id="smoothmenu">
  						<ul id="nav" class="menu">
  							<li class="menu-item">
  								<a href="#">Home</a>
  							</li>
  							<li class="menu-item">
  								<a href="#">O que fazemos</a>
  								<ul class="sub-menu">
  									<li>
  										<a href="#">Criação de Sites</a>
  									</li>
  									<li>
  										<a href="#">Sistemas Web</a>
  									</li>

  									<li>
  										<a href="#">E-commerce</a>
  									</li>

  									<li>
  										<a href="#">Aplicativos Mobile</a>
  									</li>
  								</ul>
  							</li>
  							<li class="menu-item">
  								<a href="#">Sobre nós</a>
  							</li>
  							<li class="menu-item">
  								<a href="#">Portfólio</a>
  							</li>
  							<li class="menu-item">
  								<a href="#">Blog</a>
  							</li>
  							<li class="menu-item destaque">
  								<a href="#">Contato</a>
  							</li>
  						</ul>
  					</div>
  				</div>
  			</div>
  		</header>
  		<!-- Fim Header -->

  		<!-- Banner Home -->
  		<div class="slider">
  			<div class="container">
  				<div class="col-md-12">
  					<div class="banner_home">
  						<ul>
  							<li><img src="images/banners/criacao_de_sites.png"></li>
  							<li><img src="images/banners/sistemas_web.png"></li>
  							
  						</ul>
  					</div>
  				</div>
  			</div>
  		</div>
  		<!-- Fim Banner Home -->

  		<div class="box-arrow box-dark">
			<div class="container">
				<div class="col-md-4">
					<h2>Como <b>PODEMOS AJUDAR?</b></h2>

					<p>A Codemax desenvolve sites com o objetivo de atender todos os usuários e tendências tecnológicas, ou seja, nossos layouts são modernos e se adaptam para computadores, tablets e celulares.</p>

					<a class="btn btn-vertodos">
						<i class="fa fa-cogs"></i>
						Ver todos os serviços
					</a>
				</div>
				<div class="col-md-8 nossos_servicos">
					<div class="col-md-4 text-center">
						<div class="imagem">
							<!--<i class="flaticon-responsive-design-symbol"></i>-->
							<img src="images/ico_sites.png">
						</div>
						<h1>Sites e <b>Portais</b></h1>
						<p>Loja Virtual, Landing Pages, Site institucional, Site Adulto, Blog, Portal de Conteúdo.</p>
					</div>

					<div class="col-md-4 text-center">
						<div class="imagem">
							<!--<i class="flaticon-web"></i>-->
							<img src="images/ico_sistemas.png" style="margin: 29px 0px 25px;">
						</div>
						<h1>Sistemas <b>Web</b></h1>
						<p>Projetos personalizados de sistemas e aplicativos, Intranet, ERP, CRM, HelpDesk.</p>
					</div>

					<div class="col-md-4 text-center">
						<div class="imagem">
							<!--<i class="flaticon-shopping-basket"></i>-->
							<img src="images/ico_loja2.png">
						</div>
						<h1>Loja <b>Virtual</b></h1>
						<p>Soluções de E-commerce com as melhores plataformas do mercado e pagamentos.</p>
					</div>
				</div>
			</div>
		</div>

  		<div class="services">
  			<div class="container">
  				<div class="col-md-12 text-center">
  					<h2>Últimos Trabalhos</h2>
  					<p>Confira os últimos projetos desenvolvidos pela nossa equipe.</p>
  				</div>
  			</div>

	  		<div class="portfolio">
	  			<div class="container">
	  				<div class="col-md-3 col-xs-6 projeto">
	  					<a href="#" class="thumbnail">
				      		<img src="images/portfolio/p_1.jpg" alt="sadsad">
				    	</a>

				    	<div class="caption">
					        <h3>Bling!</h3>
					        <span class="label">Sistema</span>
					    </div>
	  				</div>

	  				<div class="col-md-3 col-xs-6 projeto">
	  					<a href="#" class="thumbnail">
				      		<img src="images/portfolio/p_2.jpg" alt="sadsad">
				    	</a>
				    	<div class="caption">
					        <h3>AlfaTrac</h3>
					        <span class="label">Site</span>
					    </div>
	  				</div>

	  				<div class="col-md-3 col-xs-6 projeto">
	  					<a href="#" class="thumbnail">
				      		<img src="images/portfolio/p_3.jpg" alt="sadsad">
				    	</a>
				    	<div class="caption">
					        <h3>Haras Monte Olivetti</h3>
					        <span class="label">Site</span>
					    </div>
	  				</div>

	  				<div class="col-md-3 col-xs-6 projeto">
	  					<a href="#" class="thumbnail">
				      		<img src="images/portfolio/p_4.jpg" alt="sadsad">
				    	</a>
				    	<div class="caption">
					        <h3>Vivace</h3>
					        <span class="label">Site</span>
					    </div>
	  				</div>
	  			</div>
	  		</div>
  		</div>
  		

  		<div class="clientes">
  			<div class="container">
  				<div class="col-md-12 text-center">
  					<h2>Alguns Clientes</h2>
  					<p>Conheça algumas empresas que acreditaram em nosso potencial e hoje fazem a diferença</p>
  				</div>
  			</div>

  			<div class="container">
  				<div class="col-md-12 scroll_clientes">
  					<div class="col-md-2 cliente">
  						<img src="images/clientes/nespresso.png" class="img-responsive">
  					</div>
  					<div class="col-md-2 cliente">
  						<img src="images/clientes/nespresso.png" class="img-responsive">
  					</div>
  					<div class="col-md-2 cliente">
  						<img src="images/clientes/nespresso.png" class="img-responsive">
  					</div>
  					<div class="col-md-2 cliente">
  						<img src="images/clientes/nespresso.png" class="img-responsive">
  					</div>
  					<div class="col-md-2 cliente">
  						<img src="images/clientes/nespresso.png" class="img-responsive">
  					</div>
  					<div class="col-md-2 cliente">
  						<img src="images/clientes/nespresso.png" class="img-responsive">
  					</div>
  				</div>
  			</div>
  		</div>

  		<footer>
  			<div class="container">
  				<div class="col-md-8">
  					<div class="col-md-4 noPadding-left social">
	  					<h3>SOBRE NÓS</h3>

	  					<ul>
	  						<li><a href="#">Fábrica de Software</a></li>
	  						<li><a href="#">Agência Digital</a></li>
	  						<li><a href="#">Tecnologia da Informação</a></li>
	  					</ul>
	  				</div>

	  				<div class="col-md-4 noPadding-left social">
	  					<h3>NOSSO SERVIÇOS</b></h3>

	  					<ul>
	  						<li><a href="#">Criação de sites</a></li>
	  						<li><a href="#">Hotsites Promocionais</a></li>
	  						<li><a href="#">Sistemas Web</a></li>
	  						<li><a href="#">GerentePRO</a></li>
	  						<li><a href="#">Hospedagem de Sites</a></li>
	  						<li><a href="#">Consultoria em SEO</a></li>
	  						<li><a href="#">E-mail Marketing</a></li>
	  					</ul>
	  				</div>

	  				<div class="col-md-4 noPadding-left social">
	  					<h3>BLOG</h3>

	  					<ul>
	  						<li><a href="#">Notícias</a></li>
	  						<li><a href="#">Dicas</a></li>
	  						<li><a href="#">Tutoriais</a></li>
	  						<li><a href="#">Video Aulas</a></li>
	  					</ul>
	  				</div>
  				</div>

  				<div class="col-md-4 noPadding-left social">
  					<!--<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Ffacebook.com%2Fcodemaxbr&width=250&height=240&colorscheme=dark&show_faces=true&header=false&stream=false&show_border=false&connections=8" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:300px; height:240px;" allowtransparency="true"></iframe>-->
  					
  					<div class="row">
  						<div class="col-md-12">
  							<h3>ASSINE NOSSA NEWSLETTER</h3>

  							<p>
								Assine nossa newsletter e fique por dentro de novidades e dicas sobre sites e desenvolvimento.
							</p>


							<div class="col-md-10 noPadding">
								<div class="form-group">
					    			<input type="email" class="form-control" placeholder="jane.doe@example.com">
						  		</div>
							</div>
							<div class="col-md-2 noPadding">
								<button type="submit" class="btn btn-primary">OK</button>
							</div>

  						</div>

  						<div class="col-md-12">
  							<b>Siga-nos</b>
  							<div class="clearfix"></div>
  							<a href="#" class="redes_sociais">
		  						<i class="fa fa-facebook-official" aria-hidden="true"></i>
		  					</a>
		  					<a href="#" class="redes_sociais">
		  						<i class="fa fa-twitter" aria-hidden="true"></i>
		  					</a>
		  					<a href="#" class="redes_sociais">
		  						<i class="fa fa-github" aria-hidden="true"></i>
		  					</a>
		  					<a href="#" class="redes_sociais">
		  						<i class="fa fa-linkedin" aria-hidden="true"></i>
		  					</a>
		  					<a href="#" class="redes_sociais">
		  						<i class="fa fa-youtube" aria-hidden="true"></i>
		  					</a>
  						</div>
  					</div>


  					
  				</div>
  			</div>
  		</footer>

  		<div class="footer">
  			<div class="container">
  				<div class="col-md-2">
  					<img src="images/logo.png" class="img-responsive logo_footer" />
  				</div>
  				<div class="col-md-5 noPadding">
  					<p>Copyright 2016 &copy; Todos os direitos reservados.</p>
  				</div>

				<div class="col-md-3 pull-right text-right">
					
					<img class="img-responsive selos pull-right" src="images/selo_abradirj.png" />
					<!--<img class="img-responsive selos_mini pull-right" src="images/abraweb.png" />-->
					<p class="associado">Associado:</p>
				</div>
  			</div>
  		</div>

  		<script type="text/javascript" src="js/jquery.min.js"></script>
  		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/unslider.min.js"></script>

		<script>
			$(function(){
				var slider = $('.banner_home').unslider({
					infinite: true,
					autoplay: true,
					arrows: false,
					delay: 5000
				});

				$('.unslider-nav li').html('');
			});
		</script>
  	</body>
</html>
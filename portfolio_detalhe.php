<!DOCTYPE html>
<html lang="pt-br">
  	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Codemax Sistemas - Criação de Sites, WordPress, Sistemas Web e Projetos Customizados</title>

		<meta name="abstract" content="criação de sites | criação de sites wordpress| criação de sites rj| criação de sites no rj| criação de sites rio| criação de sites rio de janeiro| criação de sites no rio de janeiro| criação de websites | criação de websites rj| criação de websites rio de janeiro| criar site| criamos site| criamos sites| como criar um site| criação de blog| criação de portal| criação de portais| criação de site responsivo| criação de site gerenciável| criação de site empresarial| agência de criação de sites| agência de criação de sites rj| agência de criação de sites rio de janeiro| agencia web| empresa sites| empresa que faz site| empresa de sites| desenvolvimento de sites| desenvolvimento de sites rj| construção de sites| construção de websites| criação de loja virtual| criação de e-commerce| criação de lojas virtuais| desenvolvimento web| atualização de sites| manutenção de sites| criação de sites institucionais| sites empresariais| sites profissionais| quanto custa um site| valores sites| empresa de criação de site| websites dinâmicos| site para empresas| web designer| web design| sites gerenciáveis| site para advogados| site para clinicas| site para escritórios| hospedagem| hospedagem de sites| registro de domínio| atualização e manutenção de sites">

		<meta name="keywords" content="criação de sites, criação de sites wordpress, criação de sites rj, criação de sites no rj, criação de sites rio, criação de sites rio de janeiro, criação de sites no rio de janeiro, criação de websites, criação de websites rj, criação de websites rio de janeiro, criar site, criamos site, criamos sites, como criar um site, criação de blog, criação de portal, criação de portais, criação de site responsivo, criação de site gerenciável, criação de site empresarial, agência de criação de sites, agência de criação de sites rj, agência de criação de sites rio de janeiro, agencia web, empresa sites, empresa que faz site, empresa de sites, desenvolvimento de sites, desenvolvimento de sites rj, construção de sites, construção de websites, criação de loja virtual, criação de e-commerce, criação de lojas virtuais, desenvolvimento web, atualização de sites, manutenção de sites, criação de sites institucionais, sites empresariais, sites profissionais, quanto custa um site, valores sites, empresa de criação de site, websites dinâmicos, site para empresas, web designer, web design, sites gerenciáveis, site para advogados, site para clinicas, site para escritórios, hospedagem, hospedagem de sites, registro de domínio, atualização e manutenção de sites." />

		<meta name="description" content="Criação de Sites e Desenvolvimento de Sistemas em Rio de Janeiro, Itaboraí, São Gonçalo, Niterói, RJ, SP, São Paulo">

		<meta name="title" content="Empresa de criação de sistes no rio de janeiro - contato@codemax.com.br - especialista em wordpress - loja magento - Centro de São Gonçalo">

        <meta name="robots" content="index,follow">
        <meta name='target' content="all">
        <meta name="googlebot" content="index,follow">
        <meta name="author" content="Codemax Sistemas | contato@codemax.com.br" />
        <meta name="author-url" content="http://www.codemax.com.br" />
        <meta name="url" content="http://www.codemax.com.br">

		<meta name="identifier-URL" content="http://www.codemax.com.br">
        <meta name="organization name" content="Codemax Sistemas - contato@codemax.com.br">
        <meta name="revisit-after" content="2 days"/>
		<meta name="category" content="internet">
		<meta name="language" content="Portuguese">
		<meta name="doc-type" content="Web Page">
		<meta name="doc-rights" content="Copywritten Work">
		<meta name="resource-type" content="document">
		<meta name="classification" content="Sites">
		<meta name="classification" content="Sistemas">
		<meta name="classification" content="Aplicativos Mobile">
		<meta name="classification" content="Wordpress">
		<meta name="msvalidate.01" content="23DD47A1C7126648335234EFC43EC77E" />
		<meta name="google-site-verification" content="h6njSq7FvqbxhDax8qHVaOeqnmNzgZAsN3LFWhBwpxo" />
		<meta name="location" content="Rio de Janeiro | Brazil">
		<link rel="alternate" type="application/xml" title="Sitemap" href="http://www.codemax.com.br/sitemap.xml">
		<link rel="alternate" type="application/html" title="HTML" href="http://www.codemax.com.br/sitemap.html">
		<link rel="canonical" href="http://www.codemax.com.br" />
		<meta http-equiv="Expires" content="0">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Cache-Control" content="no-cache">

		<meta property="og:title" content="Criação de Sites no Rio de Janeiro - Wordpress - Sistemas web">
		<meta property="og:type" content="website">
		<meta property="og:locale" content="pt_BR">
		<meta property="og:url" content="http://www.codemax.com.br">
		<meta property="og:image" content="http://www.codemax.com.br/images/logo.png">
		<meta property="og:site_name" content="Codemax Sistemas">
		<meta property="fb:app_id" content="262843197416323">
		<meta property="fb:page_id" content="codemaxbr" />
		<meta property="og:description" content="Empresa de Criação de Sites e Desenvolvimento de Sites no Rio de Janeiro RJ, Entregamos seu site em até 7 dias, Especialistas em WordPress e Sistemas Web">
		<meta property="og:locality" content="Rio de Janeiro">
		<meta property="og:region" content="RJ">
		<meta property="og:country-name" content="Brasil">
		<meta property="og:email" content="contato@codemax.com.br">
		<meta property="og:phone_number" content="+55 21 9681-7475">

		<meta name="twitter:site" content="http://www.codemax.com.br"/>
		<meta name="twitter:domain" content="@codemaxbr"/>
		<meta name="twitter:creator" content="Codemax Sistemas"/>
		<meta name="twitter:image:src" content="http://www.codemax.com.br/images/logo.png"/>

		<meta name="norton-safeweb-site-verification" content="6vnso78y3r6l3q3q43dog8juxvq4l60kjq44omvnu5fzkj1rbbvsva6rvxsy55qatdi11w-nto36o5flrf96uq8jm8cddzodxf2ienhv0vu9zq-pkvzse0lm-nx5rw0h" />

        <link rel="shortcut icon" href="images/favicon.ico">

		<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700,900,400italic' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Raleway:500,700,400' rel='stylesheet' type='text/css'>

		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="style.css">
		<link rel="stylesheet" type="text/css" href="fonts/flaticon.css">
		<link rel="stylesheet" type="text/css" href="css/unslider.css">

		<!-- HTML5 shim e Respond.js para suporte no IE8 de elementos HTML5 e media queries -->
		<!-- ALERTA: Respond.js não funciona se você visualizar uma página file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
  	</head>
  	<body>
  		<!-- Header -->
  		<header>
  			<div class="container">
  				<div class="col-md-4">
  					<img class="img-responsive" src="images/logo.png" />
  				</div>

  				<div class="col-md-8">
  					<div class="top_contact text-right">
  						<a href="#"><i class="fa fa-facebook-square"></i></a>
  						<a href="#"><i class="fa fa-skype"></i></a>
  						<a href="#"><i class="fa fa-whatsapp"></i></a>

  						(21) 97681-7475
  					</div>

  					<div class="ddsmoothmenu pull-right" id="smoothmenu">
  						<ul id="nav" class="menu">
  							<li class="menu-item">
  								<a href="#">Home</a>
  							</li>
  							<li class="menu-item">
  								<a href="#">O que fazemos</a>
  								<ul class="sub-menu">
  									<li>
  										<a href="#">Criação de Sites</a>
  									</li>
  									<li>
  										<a href="#">Sistemas Web</a>
  									</li>

  									<li>
  										<a href="#">E-commerce</a>
  									</li>

  									<li>
  										<a href="#">Aplicativos Mobile</a>
  									</li>
  								</ul>
  							</li>
  							<li class="menu-item">
  								<a href="#">Sobre nós</a>
  							</li>
  							<li class="menu-item">
  								<a href="#">Portfólio</a>
  							</li>
  							<li class="menu-item">
  								<a href="#">Blog</a>
  							</li>
  							<li class="menu-item destaque">
  								<a href="#">Contato</a>
  							</li>
  						</ul>
  					</div>
  				</div>
  			</div>
  		</header>
  		<!-- Fim Header -->

  		<!--
  		<div class="banner_page">
  			<div class="img_banner" style="background:url(images/banner_empresa.jpg) no-repeat center top;"></div>
  		</div>
  		-->

  		<div class="box-titpage">
			<div class="container">
				<div class="col-md-12">
					<h1>AlfaTrac</h1>
				</div>
			</div>
		</div>

        <div class="porfolio_detalhes">
            <div class="container">
                <div class="col-md-7">
                    <img class="img-responsive" src="images/portfolio/3.jpg" />
                </div>

                <div class="col-md-5">
                    <h1>AlfaTrac</h1>

                    <p>
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                    </p>

                    <p><b>Serviço: </b>Criação de Site e Desenvolvimento de Plugin Wordpress</p>
                    <p><b>Tecnologias: </b>PHP, MySQL, Wordpress, Lighbox e jQuery</p>
                </div>
            </div>
        </div>

  		<footer>
  			<div class="container">
  				<div class="col-md-3 about">
  					<img src="images/logo.png"/>
  					<p>Sed ut perspiciatis unde omnis iste natus error sit volup accusantium lorem. Lorem ipsum dolor sit amet, consectetur.</p>

  					<span class="address">77a First Street, London, UK</span>
  					<span class="email">contact@copro.com</span>
  					<span class="phone">+55 21 4063-5062</span>
  				</div>

  				<div class="col-md-3 social">
  					<h3>FOLLOW US <b>@codemaxbr</b></h3>
  				</div>

  				<div class="col-md-3">
  					ss
  				</div>

  				<div class="col-md-3">
  					ss
  				</div>
  			</div>
  		</footer>

  		<script type="text/javascript" src="js/jquery.min.js"></script>
  		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/unslider.min.js"></script>

		<script>
			$(function(){
				var slider = $('.banner_home').unslider({
					infinite: true,
					autoplay: true,
					arrows: false,
					delay: 5000
				});

				$('.unslider-nav li').html('');
			});
		</script>
  	</body>
</html>